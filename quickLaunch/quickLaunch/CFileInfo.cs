﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using System.Drawing;


namespace getFile
{
    public class CFileInfo
    {
        private string _path, _name;
        private int _count;
        public Icon _icon;
        public CFileInfo(string path, string name)
        {
            _path = path;
            _name = name;
            _count = 0;
        }

        public CFileInfo(string path, string name, int count)
        {
            _path = path;
            _name = name;
            _count = count;
        }
        public string getPath()
        {
            return _path;
        }
        public string getName()
        {
            return _name;
        }

        public int getCount()
        {
            return _count;
        }

        public bool setCount(int count = 1)
        {
            if (count > 0)
            {
                this._count += count;
                return true;
            }
            return false;
        }
        public CFileInfo(FileInfo file)
        {
            _name = file.Name;
            _path = file.DirectoryName;
            _count = 0;
        }

        //public int CompareTo(CFileInfo cfile)
        //{
        //    string name = cfile.getName();
        //    return string.Compare(_name, name);
        //}
        public class CountComparer : IComparer<CFileInfo>
        {
            public int Compare(CFileInfo x, CFileInfo y)
            {
                int num1 = x.getCount(), num2 = y.getCount();
                if (num1 > num2)
                    return -1;
                else if (num1 < num2)
                    return 1;
                return 0;
            }
        }

        public class NameComparer : IComparer<CFileInfo>
        {
            public int Compare(CFileInfo x, CFileInfo y)
            {
                string nameX = x.getName();
                string nameY = y.getName();
                return (string.Compare(nameX, nameY));
            }
        }
    }
}