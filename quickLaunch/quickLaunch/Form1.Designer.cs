﻿namespace quickLaunch
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Menu));
            this.mainPanel = new System.Windows.Forms.Panel();
            this.panelSearch = new System.Windows.Forms.Panel();
            this.tbSearch = new System.Windows.Forms.TextBox();
            this.picSearch = new System.Windows.Forms.PictureBox();
            this.menuPanel = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.picExit = new System.Windows.Forms.PictureBox();
            this.panelExit = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.picClear = new System.Windows.Forms.PictureBox();
            this.panelScan = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.picScan = new System.Windows.Forms.PictureBox();
            this.panelAddScan = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.picAddScan = new System.Windows.Forms.PictureBox();
            this.panelStatis = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.statis = new System.Windows.Forms.PictureBox();
            this.panelHistory = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.picHistory = new System.Windows.Forms.PictureBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.showMenuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statitisticToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addNewFolderToScanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.listViewApp = new System.Windows.Forms.ListView();
            this.mainPanel.SuspendLayout();
            this.panelSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picSearch)).BeginInit();
            this.menuPanel.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picExit)).BeginInit();
            this.panelExit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picClear)).BeginInit();
            this.panelScan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picScan)).BeginInit();
            this.panelAddScan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picAddScan)).BeginInit();
            this.panelStatis.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.statis)).BeginInit();
            this.panelHistory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picHistory)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainPanel
            // 
            this.mainPanel.BackColor = System.Drawing.Color.WhiteSmoke;
            this.mainPanel.Controls.Add(this.panelSearch);
            this.mainPanel.Controls.Add(this.menuPanel);
            this.mainPanel.Location = new System.Drawing.Point(2, 0);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(493, 453);
            this.mainPanel.TabIndex = 0;
            // 
            // panelSearch
            // 
            this.panelSearch.BackColor = System.Drawing.Color.White;
            this.panelSearch.Controls.Add(this.tbSearch);
            this.panelSearch.Controls.Add(this.picSearch);
            this.panelSearch.Location = new System.Drawing.Point(3, 88);
            this.panelSearch.Name = "panelSearch";
            this.panelSearch.Size = new System.Drawing.Size(480, 49);
            this.panelSearch.TabIndex = 2;
            // 
            // tbSearch
            // 
            this.tbSearch.Location = new System.Drawing.Point(42, 13);
            this.tbSearch.Name = "tbSearch";
            this.tbSearch.Size = new System.Drawing.Size(435, 20);
            this.tbSearch.TabIndex = 2;
            this.tbSearch.TextChanged += new System.EventHandler(this.tbSearch_TextChanged);
            this.tbSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Menu_KeyDown);
            this.tbSearch.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Menu_KeyUp);
            // 
            // picSearch
            // 
            this.picSearch.Image = global::quickLaunch.Properties.Resources.iconSearch;
            this.picSearch.Location = new System.Drawing.Point(19, 13);
            this.picSearch.Name = "picSearch";
            this.picSearch.Size = new System.Drawing.Size(17, 20);
            this.picSearch.TabIndex = 1;
            this.picSearch.TabStop = false;
            // 
            // menuPanel
            // 
            this.menuPanel.BackColor = System.Drawing.SystemColors.Menu;
            this.menuPanel.Controls.Add(this.panel2);
            this.menuPanel.Controls.Add(this.panelExit);
            this.menuPanel.Controls.Add(this.panelScan);
            this.menuPanel.Controls.Add(this.panelAddScan);
            this.menuPanel.Controls.Add(this.panelStatis);
            this.menuPanel.Controls.Add(this.panelHistory);
            this.menuPanel.Location = new System.Drawing.Point(0, 0);
            this.menuPanel.Name = "menuPanel";
            this.menuPanel.Size = new System.Drawing.Size(492, 82);
            this.menuPanel.TabIndex = 1;
            this.menuPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.menuPanel_Paint);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.picExit);
            this.panel2.Location = new System.Drawing.Point(408, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(75, 75);
            this.panel2.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(24, 56);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(24, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Exit";
            // 
            // picExit
            // 
            this.picExit.Image = global::quickLaunch.Properties.Resources.icons8_exit_sign_50;
            this.picExit.Location = new System.Drawing.Point(12, 3);
            this.picExit.Name = "picExit";
            this.picExit.Size = new System.Drawing.Size(50, 50);
            this.picExit.TabIndex = 0;
            this.picExit.TabStop = false;
            this.picExit.Click += new System.EventHandler(this.pictureBox1_Click);
            this.picExit.MouseLeave += new System.EventHandler(this.picExit_MouseLeave);
            this.picExit.MouseHover += new System.EventHandler(this.picExit_MouseHover);
            // 
            // panelExit
            // 
            this.panelExit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panelExit.Controls.Add(this.label5);
            this.panelExit.Controls.Add(this.picClear);
            this.panelExit.Location = new System.Drawing.Point(327, 3);
            this.panelExit.Name = "panelExit";
            this.panelExit.Size = new System.Drawing.Size(75, 75);
            this.panelExit.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Clear History";
            // 
            // picClear
            // 
            this.picClear.Image = global::quickLaunch.Properties.Resources.icons8_process_50;
            this.picClear.Location = new System.Drawing.Point(12, 3);
            this.picClear.Name = "picClear";
            this.picClear.Size = new System.Drawing.Size(50, 50);
            this.picClear.TabIndex = 0;
            this.picClear.TabStop = false;
            this.picClear.Click += new System.EventHandler(this.picClear_Click);
            this.picClear.MouseClick += new System.Windows.Forms.MouseEventHandler(this.picExit_MouseClick);
            this.picClear.MouseLeave += new System.EventHandler(this.picClear_MouseLeave);
            this.picClear.MouseHover += new System.EventHandler(this.picClear_MouseHover);
            // 
            // panelScan
            // 
            this.panelScan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panelScan.Controls.Add(this.label4);
            this.panelScan.Controls.Add(this.picScan);
            this.panelScan.Location = new System.Drawing.Point(246, 3);
            this.panelScan.Name = "panelScan";
            this.panelScan.Size = new System.Drawing.Size(75, 75);
            this.panelScan.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Scan";
            // 
            // picScan
            // 
            this.picScan.Image = global::quickLaunch.Properties.Resources.icons8_search_50;
            this.picScan.Location = new System.Drawing.Point(12, 3);
            this.picScan.Name = "picScan";
            this.picScan.Size = new System.Drawing.Size(50, 50);
            this.picScan.TabIndex = 0;
            this.picScan.TabStop = false;
            this.picScan.Click += new System.EventHandler(this.picScan_Click);
            this.picScan.MouseLeave += new System.EventHandler(this.picScan_MouseLeave);
            this.picScan.MouseHover += new System.EventHandler(this.picScan_MouseHover);
            // 
            // panelAddScan
            // 
            this.panelAddScan.BackColor = System.Drawing.SystemColors.Window;
            this.panelAddScan.Controls.Add(this.label3);
            this.panelAddScan.Controls.Add(this.picAddScan);
            this.panelAddScan.Location = new System.Drawing.Point(165, 3);
            this.panelAddScan.Name = "panelAddScan";
            this.panelAddScan.Size = new System.Drawing.Size(75, 75);
            this.panelAddScan.TabIndex = 2;
            this.panelAddScan.MouseLeave += new System.EventHandler(this.panelAddScan_MouseLeave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Add Scan";
            // 
            // picAddScan
            // 
            this.picAddScan.BackColor = System.Drawing.SystemColors.Window;
            this.picAddScan.Image = global::quickLaunch.Properties.Resources.icons8_view_50;
            this.picAddScan.Location = new System.Drawing.Point(12, 3);
            this.picAddScan.Name = "picAddScan";
            this.picAddScan.Size = new System.Drawing.Size(50, 50);
            this.picAddScan.TabIndex = 0;
            this.picAddScan.TabStop = false;
            this.picAddScan.Click += new System.EventHandler(this.picAddScan_Click);
            this.picAddScan.MouseLeave += new System.EventHandler(this.picAddScan_MouseLeave);
            this.picAddScan.MouseHover += new System.EventHandler(this.picAddScan_MouseHover);
            // 
            // panelStatis
            // 
            this.panelStatis.BackColor = System.Drawing.Color.White;
            this.panelStatis.Controls.Add(this.label2);
            this.panelStatis.Controls.Add(this.statis);
            this.panelStatis.Location = new System.Drawing.Point(84, 3);
            this.panelStatis.Name = "panelStatis";
            this.panelStatis.Size = new System.Drawing.Size(75, 75);
            this.panelStatis.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Statistical";
            // 
            // statis
            // 
            this.statis.Image = global::quickLaunch.Properties.Resources.icons8_statistics_50;
            this.statis.Location = new System.Drawing.Point(11, 3);
            this.statis.Name = "statis";
            this.statis.Size = new System.Drawing.Size(50, 50);
            this.statis.TabIndex = 0;
            this.statis.TabStop = false;
            this.statis.Click += new System.EventHandler(this.statis_Click);
            this.statis.MouseLeave += new System.EventHandler(this.statis_MouseLeave);
            this.statis.MouseHover += new System.EventHandler(this.statis_MouseHover);
            // 
            // panelHistory
            // 
            this.panelHistory.BackColor = System.Drawing.Color.White;
            this.panelHistory.Controls.Add(this.label1);
            this.panelHistory.Controls.Add(this.picHistory);
            this.panelHistory.Location = new System.Drawing.Point(3, 3);
            this.panelHistory.Name = "panelHistory";
            this.panelHistory.Size = new System.Drawing.Size(75, 75);
            this.panelHistory.TabIndex = 0;
            this.panelHistory.MouseLeave += new System.EventHandler(this.panelHistory_MouseLeave);
            this.panelHistory.MouseHover += new System.EventHandler(this.panelHistory_MouseHover);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "History";
            // 
            // picHistory
            // 
            this.picHistory.Image = global::quickLaunch.Properties.Resources.Blue_Alaram_Clock;
            this.picHistory.Location = new System.Drawing.Point(12, 3);
            this.picHistory.Name = "picHistory";
            this.picHistory.Size = new System.Drawing.Size(50, 50);
            this.picHistory.TabIndex = 0;
            this.picHistory.TabStop = false;
            this.picHistory.Click += new System.EventHandler(this.picHistory_Click);
            this.picHistory.MouseHover += new System.EventHandler(this.picHistory_MouseHover);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showMenuToolStripMenuItem,
            this.scanToolStripMenuItem,
            this.statitisticToolStripMenuItem,
            this.addNewFolderToScanToolStripMenuItem,
            this.exitToolStripMenuItem1});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(197, 114);
            // 
            // showMenuToolStripMenuItem
            // 
            this.showMenuToolStripMenuItem.Name = "showMenuToolStripMenuItem";
            this.showMenuToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.showMenuToolStripMenuItem.Text = "Show Menu";
            this.showMenuToolStripMenuItem.Click += new System.EventHandler(this.showMenuToolStripMenuItem_Click);
            // 
            // scanToolStripMenuItem
            // 
            this.scanToolStripMenuItem.Name = "scanToolStripMenuItem";
            this.scanToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.scanToolStripMenuItem.Text = "Scan";
            this.scanToolStripMenuItem.Click += new System.EventHandler(this.scanToolStripMenuItem_Click);
            // 
            // statitisticToolStripMenuItem
            // 
            this.statitisticToolStripMenuItem.Name = "statitisticToolStripMenuItem";
            this.statitisticToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.statitisticToolStripMenuItem.Text = "Statitistic";
            this.statitisticToolStripMenuItem.Click += new System.EventHandler(this.statitisticToolStripMenuItem_Click);
            // 
            // addNewFolderToScanToolStripMenuItem
            // 
            this.addNewFolderToScanToolStripMenuItem.Name = "addNewFolderToScanToolStripMenuItem";
            this.addNewFolderToScanToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.addNewFolderToScanToolStripMenuItem.Text = "Add new folder to scan";
            this.addNewFolderToScanToolStripMenuItem.Click += new System.EventHandler(this.addNewFolderToScanToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem1
            // 
            this.exitToolStripMenuItem1.Name = "exitToolStripMenuItem1";
            this.exitToolStripMenuItem1.Size = new System.Drawing.Size(196, 22);
            this.exitToolStripMenuItem1.Text = "Exit";
            this.exitToolStripMenuItem1.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip1;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.listViewApp);
            this.panel1.Location = new System.Drawing.Point(2, 145);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(490, 298);
            this.panel1.TabIndex = 1;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // listViewApp
            // 
            this.listViewApp.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.listViewApp.FullRowSelect = true;
            this.listViewApp.GridLines = true;
            this.listViewApp.HoverSelection = true;
            this.listViewApp.Location = new System.Drawing.Point(3, 4);
            this.listViewApp.Name = "listViewApp";
            this.listViewApp.Size = new System.Drawing.Size(478, 291);
            this.listViewApp.TabIndex = 0;
            this.listViewApp.UseCompatibleStateImageBehavior = false;
            this.listViewApp.View = System.Windows.Forms.View.Details;
            this.listViewApp.SelectedIndexChanged += new System.EventHandler(this.listViewApp_SelectedIndexChanged);
            this.listViewApp.DoubleClick += new System.EventHandler(this.listViewApp_DoubleClick);
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(495, 455);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.mainPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "Menu";
            this.Text = "Quick Note";
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Menu_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Menu_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Menu_KeyUp);
            this.mainPanel.ResumeLayout(false);
            this.panelSearch.ResumeLayout(false);
            this.panelSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picSearch)).EndInit();
            this.menuPanel.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picExit)).EndInit();
            this.panelExit.ResumeLayout(false);
            this.panelExit.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picClear)).EndInit();
            this.panelScan.ResumeLayout(false);
            this.panelScan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picScan)).EndInit();
            this.panelAddScan.ResumeLayout(false);
            this.panelAddScan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picAddScan)).EndInit();
            this.panelStatis.ResumeLayout(false);
            this.panelStatis.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.statis)).EndInit();
            this.panelHistory.ResumeLayout(false);
            this.panelHistory.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picHistory)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.Panel panelSearch;
        private System.Windows.Forms.Panel menuPanel;
        private System.Windows.Forms.Panel panelHistory;
        private System.Windows.Forms.PictureBox picHistory;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panelStatis;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox statis;
        private System.Windows.Forms.Panel panelAddScan;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox picAddScan;
        private System.Windows.Forms.Panel panelScan;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox picScan;
        private System.Windows.Forms.PictureBox picSearch;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem showMenuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem scanToolStripMenuItem;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.TextBox tbSearch;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panelExit;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox picClear;
        private System.Windows.Forms.ToolStripMenuItem statitisticToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addNewFolderToScanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem1;
        private System.Windows.Forms.ListView listViewApp;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox picExit;
    }
}

