﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace getFile
{
    public class CLibrary
    {
        public List<CFileInfo>[] hashListFile;

        private bool isNumber(char c)
        {
            if (c < '0' || c > '9')
                return false;
            return true;
        }

        private bool isCharacter(char c)
        {
            if (c >= 'a' && c <= 'z')
                return true;
            if (c >= 'A' && c <= 'Z')
                return true;
            return false;
        }

        private int hash(string src)
        {
            int hashValue = 25;
            if (isNumber(src[0]))
            {
                hashValue = src[0] - '0';
            }
            else if (isCharacter(src[0]))
            {
                hashValue = src[0] - 'a';
                if (hashValue < 0)
                {
                    hashValue += 32;
                }
            }
            return hashValue;
        }

        private void addItem(FileInfo file) {
            CFileInfo item = new CFileInfo(file);
            string name = item.getName();
            int index = hash(name);
            hashListFile[index].Add(item);
        }

        public CFileInfo find(string path, string name)
        {
            int index = hash(name);
            CFileInfo file = this.hashListFile[index].Find(x => x.getPath() == path && x.getName() == name);
            return file;
        }
        private void Scan(DirectoryInfo dir = null)
        {
            if (dir == null)
            {
                dir = new DirectoryInfo(System.Environment
                      .GetFolderPath(System.Environment.SpecialFolder.CommonApplicationData));
            }

            try
            {
                var dirs = dir.GetDirectories();
                foreach (var subDir in dirs)
                    Scan(subDir);

                var files = dir.GetFiles("*.exe", SearchOption.TopDirectoryOnly);
                foreach (var file in files)
                {
                    addItem(file);
                }

            }
            catch (UnauthorizedAccessException)
            {
                // log error
            }
        }

        public CLibrary()
        {
            hashListFile = new List<CFileInfo>[26];
            for (int i = 0; i < 26; i++)
            {
                hashListFile[i] = new List<CFileInfo>();
            }
        }
        public CLibrary(string path)
        {
            DirectoryInfo dir = new DirectoryInfo(path);
            Scan(dir);
            for (int i = 0; i < 26; i++)
            {
                hashListFile[i].Sort();
            }
        }

        public void updateScanApp(string path)
        {
            DirectoryInfo dir = new DirectoryInfo(path);
            Scan(dir);
            for (int i = 0; i < 26; i++)
            {
                hashListFile[i].Sort(new CFileInfo.NameComparer());
            }
        }

        public List<CFileInfo> find(string text)
        {
            int index = hash(text);
            int size = hashListFile[index].Count;
            List<CFileInfo> list = hashListFile[index];
            List<CFileInfo> listRes = new List<CFileInfo>();
            for (int i = 0; i < size; i++)
            {
                int Length = text.Length;
                string name = list[i].getName();
                string subStr = "";
                if (name.Length < Length)
                    continue;
                else
                {
                    subStr = list[i].getName().Substring(0, text.Length);
                }
                    
                if (subStr.CompareTo(text) == 0)
                {
                    listRes.Add(list[i]);
                }
            }
            //list.Clear();
            return listRes;
        }
    }
}
