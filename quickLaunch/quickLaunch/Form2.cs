﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using getFile;

namespace quickLaunch
{
    public partial class Sta : Form
    {
        public Sta()
        {
            InitializeComponent();
        }

        private List<CFileInfo> _list;

        public Sta(List<CFileInfo> list): this()
        {
            _list = new List<CFileInfo>(list);
            DrawChart();
        }

        private void DrawChart()
        {
            chart.Series.Clear();
            chart.Legends.Clear();

            chart.Legends.Add("MyLegend");

            string seriesname = "Quick Access";
            chart.Series.Add(seriesname);

            foreach (CFileInfo x in _list)
            {
                chart.Series[seriesname].Points.AddXY(x.getName(), x.getCount());
            }
        }
    }
}
