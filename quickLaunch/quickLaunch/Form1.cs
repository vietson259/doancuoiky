﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using getFile;
using System.Diagnostics;
using System.IO;

using System.Drawing;
namespace quickLaunch
{
    public partial class Menu : Form
    {
        private CLibrary libApp;
        private List<CFileInfo> libAppHistory;
        public Menu()
        {
            InitializeComponent();
            libApp = new CLibrary();
            libAppHistory = new List<CFileInfo>();
            load();
        }

        private void showMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            this.ShowInTaskbar = true;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void panelHistory_MouseHover(object sender, EventArgs e)
        {
            ToolTip tTip = new ToolTip();
            tTip.SetToolTip(this.panelHistory, "Ctrl + O");
        }

        bool CtrKey = false;
        bool SKey = false;

        bool EnterKey = false;

        private void picHistory_Click(object sender, EventArgs e)
        {
            this.tbSearch.Text = "";
            showListApp(libAppHistory);
        }

        private void Menu_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.ControlKey)
                CtrKey = true;
            if (e.KeyCode == Keys.S)
                SKey = true;
            if (e.KeyCode == Keys.Enter)
                SKey = true;
            if (e.KeyCode == Keys.Enter)
            {
                listViewApp_DoubleClick(sender, e);
                EnterKey = false;
            }
            if (CtrKey && SKey)
            {
                Sta f = new Sta();
                f.Show();
                SKey = CtrKey = false;
            }
        }

        private void Menu_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.ControlKey)
                CtrKey = false;
            if (e.KeyCode == Keys.S)
                SKey = false;
            if (e.KeyCode == Keys.Enter)
                EnterKey = false;
        }

        private void scanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            picScan_Click(sender, e);
        }

        private void picAddScan_Click(object sender, EventArgs e)
        {
            string path = this.tbSearch.Text;
            if (Directory.Exists(path) && path != "")
            {
                listViewApp.Clear();
                libApp.updateScanApp(path);
                showApp();
                MessageBox.Show("The path is correct!");
            }
            else
            {
                MessageBox.Show("The path is not correct!");
            }
        }

        public delegate void SendMessage(string mess);
        public SendMessage sender;

        private void showListApp(List<CFileInfo> list)
        {
            listViewApp.Clear();
            listViewApp.Columns.Add("Application", 200);
            listViewApp.Columns.Add("Path", 400);

            List<CFileInfo> listCopy = new List<CFileInfo>(list);
            listCopy.Sort(new CFileInfo.CountComparer());
            int size = listCopy.Count();

            ListViewItem[] item = new ListViewItem[size];
            
            for (int j = 0; j < size; j++)
            {
                item[j] = new ListViewItem();
                item[j].Text = listCopy[j].getName();
                item[j].SubItems.Add(new ListViewItem.ListViewSubItem() { Text = list[j].getPath() });
                this.listViewApp.Items.Add(item[j]);
            }
            listCopy.Clear();
        }
        private void showApp()
        {
            listViewApp.Clear();
            listViewApp.Columns.Add("Application", 200);
            listViewApp.Columns.Add("Path", 400);

            List<CFileInfo> list = new List<CFileInfo>();

            int size = 0;
            for (int i = 0; i < 26; i++)
            {
                size += libApp.hashListFile[i].Count();
                list.AddRange(libApp.hashListFile[i]);
            }

            ListViewItem[] item = new ListViewItem[size];

            for (int j = 0; j < size; j++)
            {
                item[j] = new ListViewItem();
                item[j].Text = list[j].getName();
                item[j].SubItems.Add(new ListViewItem.ListViewSubItem() { Text = list[j].getPath() });
                this.listViewApp.Items.Add(item[j]);
            }
        }

        private void picScan_Click(object sender, EventArgs e)
        {
            //Form f = new wait();
            //f.Show();
            MessageBox.Show("Please wait, just a moment!");
            this.libApp.updateScanApp(@"C:\Program Files (x86)");
            listViewApp.Columns.Add("Application", 150);
            listViewApp.Columns.Add("Path", 350);

            int size = libApp.hashListFile[1].Count();
            ListViewItem[] item = new ListViewItem[size];

            List<CFileInfo> list = new List<CFileInfo>();
            list = libApp.hashListFile[1].GetRange(0, size);

            for (int j = 0; j < size; j++)
            {
                item[j] = new ListViewItem();
                item[j].Text = list[j].getName();
                item[j].SubItems.Add(new ListViewItem.ListViewSubItem() { Text = list[j].getPath() });
                this.listViewApp.Items.Add(item[j]);
            }
            //f.Close();
        }

        private void panelAddScan_MouseLeave(object sender, EventArgs e)
        {
            picAddScan.BackColor = Color.White;
            panelAddScan.BackColor = Color.White;
        }

        private void picScan_MouseHover(object sender, EventArgs e)
        {
            picScan.BackColor = Color.LightBlue;
            panelScan.BackColor = Color.LightBlue;
        }

        private void picScan_MouseLeave(object sender, EventArgs e)
        {
            picScan.BackColor = Color.White;
            panelScan.BackColor = Color.White;
        }

        private void listViewApp_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void listViewApp_DoubleClick(object sender, EventArgs e)
        {
            ListView lsv = this.listViewApp;
            string path = lsv.SelectedItems[0].SubItems[1].Text;
            string name = lsv.SelectedItems[0].Text;
            if (path != "" && name != "")
            {
                string temp = path + "\\" + name;
                //MessageBox.Show(temp);
                Process.Start(temp);

                CFileInfo file = libApp.find(path, name);
                file.setCount();
                add(file);
            }
        }

        private void add(CFileInfo file)
        {
            if (!libAppHistory.Any(x => x.getPath() == file.getPath() && x.getName() == file.getName()))
            {
                libAppHistory.Insert(0, file);
            }
        }

        private void tbSearch_TextChanged(object sender, EventArgs e)
        {
            string text = tbSearch.Text;
            listViewApp.Clear();
            if (text != "")
            {
                List<CFileInfo> list = libApp.find(text);
                showListApp(list);
            }
        }

        private void cmdSend_Click(object sender, EventArgs e)
        {
            Sta Child = new Sta(this.libAppHistory);
            Child.Show();
        }

        private void statis_Click(object sender, EventArgs e)
        {
            cmdSend_Click(sender, e);
        }

        private void save()
        {
            FileStream fs = new FileStream("history.txt", FileMode.Create);
            StreamWriter sWriter = new StreamWriter(fs, Encoding.UTF8);
            int size = libAppHistory.Count();

            sWriter.WriteLine(size);
            foreach (CFileInfo file in libAppHistory) 
            {
                sWriter.WriteLine(file.getName());
                sWriter.WriteLine(file.getPath());
                sWriter.WriteLine(file.getCount());
            }
            sWriter.Flush();
            fs.Close();
        }

        private void load()
        {
            libAppHistory.Clear();

            using (StreamReader sr = new StreamReader("history.txt"))
            {
                string name = "", path = "", count = "";
                string ssize = sr.ReadLine();
                if (ssize == null)
                    return;
                int size = int.Parse(ssize);

                for (int i = 0; i < size; i++)
                {
                    name = sr.ReadLine();
                    path = sr.ReadLine();
                    count = sr.ReadLine();
                    CFileInfo file = new CFileInfo(path, name, int.Parse(count));
                    libAppHistory.Add(file);
                }
            }
        }

        private void Menu_FormClosing(object sender, FormClosingEventArgs e)
        {
            save();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            save();
            Application.Exit();
        }

        private void menuPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void picExit_MouseClick(object sender, MouseEventArgs e)
        {
            libAppHistory.Clear();
            listViewApp.Clear();
        }

        private void picClear_Click(object sender, EventArgs e)
        {
            libAppHistory.Clear();
            listViewApp.Clear();
        }

        private void statitisticToolStripMenuItem_Click(object sender, EventArgs e)
        {
            statis_Click(sender, e);
        }

        private void addNewFolderToScanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMenuToolStripMenuItem_Click(sender, e);
        }

        private void picHistory_MouseHover(object sender, EventArgs e)
        {
            picHistory.BackColor = Color.LightBlue;
            panelHistory.BackColor = Color.LightBlue;
        }

        private void panelHistory_MouseLeave(object sender, EventArgs e)
        {
            picHistory.BackColor = Color.White;
            panelHistory.BackColor = Color.White;
        }

        private void statis_MouseHover(object sender, EventArgs e)
        {
            statis.BackColor = Color.LightBlue;
            panelStatis.BackColor = Color.LightBlue;
        }

        private void statis_MouseLeave(object sender, EventArgs e)
        {
            statis.BackColor = Color.White;
            panelStatis.BackColor = Color.White;
        }

        private void picClear_MouseHover(object sender, EventArgs e)
        {
            picClear.BackColor = Color.LightBlue;
            panelExit.BackColor = Color.LightBlue;
        }

        private void picClear_MouseLeave(object sender, EventArgs e)
        {
            picClear.BackColor = Color.White;
            panelExit.BackColor = Color.White;
        }

        private void picExit_MouseHover(object sender, EventArgs e)
        {
            picExit.BackColor = Color.LightBlue;
            panel2.BackColor = Color.LightBlue;
        }

        private void picExit_MouseLeave(object sender, EventArgs e)
        {
            picExit.BackColor = Color.White;
            panel2.BackColor = Color.White;
        }

        private void picAddScan_MouseHover(object sender, EventArgs e)
        {
            picAddScan.BackColor = Color.LightBlue;
            panelAddScan.BackColor = Color.LightBlue;
        }

        private void picAddScan_MouseLeave(object sender, EventArgs e)
        {
            picAddScan.BackColor = Color.White;
            panelAddScan.BackColor = Color.White;
        }


    }
}
